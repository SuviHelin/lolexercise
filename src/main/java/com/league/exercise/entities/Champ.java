package com.league.exercise.entities;

import javax.persistence.*;

@Entity(name="lolchampion")
public class Champ {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="championName")
    private String name;

    @Column(name="championRole")
    private String role;

    @Column(name="difficulty")
    private String difficulty;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        difficulty = difficulty;
    }

    @Override
    public String toString() {
        return "Champ{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", Difficulty='" + difficulty + '\'' +
                '}';
    }
}
