package com.league.exercise.service;

import com.league.exercise.entities.Champ;
import com.league.exercise.repository.ChampRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChampService {

    @Autowired
    ChampRepository champRepository;

    public List<Champ> findAll(){
        return champRepository.findAll();
    }

    public Champ save(Champ champ){
        return champRepository.save(champ);
    }

    public Champ update(Champ champ) {
        champRepository.findById(champ.getId()).get();

        return champRepository.save(champ);
    }

    public void delete(long id) {
        champRepository.deleteById(id);
    }

}
