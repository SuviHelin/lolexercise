package com.league.exercise.controller;

import com.league.exercise.entities.Champ;
import com.league.exercise.service.ChampService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/champ")
public class ChampController {

    private static final Logger LOG = LoggerFactory.getLogger(ChampController.class);

    @Autowired
    private ChampService champService;

    @GetMapping
    public List<Champ> findAll(){
        return champService.findAll();
    }

    @PostMapping
    public ResponseEntity<Champ> create(@RequestBody Champ champ) {
        return new ResponseEntity<Champ>(champService.save(champ), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Champ> update(@RequestBody Champ champ) {
        try {
            return new ResponseEntity<Champ>(champService.update(champ),HttpStatus.OK);
        }catch (NoSuchElementException ex){
            LOG.debug("update for unknown id: [" + champ + "]");
            return new ResponseEntity<Champ>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id){
        try {
            champService.delete(id);
        }catch(EmptyResultDataAccessException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
